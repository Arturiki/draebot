## Context

As a *developer* I want to

*describe my usecase*

so that

*I understand what the usecase does*

## Requirements

- [ ] Fill in with requirements
- [ ] Review

## Tasks

- [ ] (1d) Fill in with clear tasks
- [ ] (1d) Fill in with clear tasks

##  Labels
~Meta 
~Bug 
~Deployment 
~Documentation
~Feature
~Visibility

## Other issues

*Related to usecase* #

----

## Examples

The **Title** follows the format "[*priority*](time) Short description"
Priority determines the hurry of the task, being A the highest and C the lowest.
Time is the expected workload (in days) to fulfill the usecase.
Example: [A](1d) Add a usecase template

The **Context** describes briefly who and why the change is needed.

The **Requirements** are a list of general procesdures to be fulfilled.

The **Tasks** describe simple univocal activities to be fulfilled within the expected time window indicated in the parenthesis. 


* [Mulitpolygon issue](https://github.com/CartoDB/cartodb-platform/issues/721)
* [Madrid Projection issue](https://github.com/CartoDB/cartodb-platform/issues/579)
* [SQL Layer deletion issue](https://github.com/CartoDB/cartodb-platform/issues/598)
* [Table Deletion Issue](https://github.com/CartoDB/cartodb-platform/issues/722)