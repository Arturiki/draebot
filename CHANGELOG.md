# Changelog

All notable changes to this project will be documented in this file.


## [v0.4] - 2020-03-07

### Added

- `/expresiones` command.
- Documentation for *browser.py*.
- Bot shows it is typing while processing commands.

### Changed

- Change `/significado` to `/definiciones` command.
- Bold title, italics description on `/definiciones` command.
- Update `/help` content.


## [v0.3] - 2020-03-06

### Added

- `/anagrama` command.
- `/significado` command.
- Browser scraping.

### Changed

- Fix `/start` spam.
- Header date format to `YYY-MM-DD HH:MM`.
- Add more log prints.


### Deleted

- Old dependencies.


## [v0.2] - 2020-02-20

### Changed

- Fix `/start` spam.
- Header dates.
- CHANGELOG.
- Merge request templates.
- Issue template.


## [v0.1] - 2020-02-16

### Added

- First code approach.
- PEP8 formatting.
- CHANGELOG.
- LICENCE.
- CONTRIBUTING.
- Usecase and merge request templates.
- Logo image.
