# -*- coding: utf-8 -*-
"""
Created on 2020-02-16 11:18

@DiccionarioRAEbot for Telegram.
Version 0.4

@author: Arturiki
"""
import logging
import os
import signal

from telegram import ParseMode, InlineKeyboardMarkup, ChatAction
from functools import wraps
from keyboard import donate_keyboard
from browser import get_result

# from time import sleep

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %('
                           'message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

bot_stopped = False
start_called = False
first_start = True
second_start = False


def send_typing_action(func):
    """Sends typing action while processing func command.

    Args:
        func: Method to be decorated.

    Returns:
        Decorated method.
        """
    @wraps(func)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(chat_id=update.effective_message.chat_id,
                                     action=ChatAction.TYPING)
        return func(update, context,  *args, **kwargs)

    return command_func


@send_typing_action
def start(update, context):
    """Salutes the user with a short description when the command /start is
    issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    chat_id = update.effective_chat.id
    username = update.message.from_user.username
    hi_message = ''

    global start_called, first_start, second_start
    if not start_called and first_start:
        # noinspection SpellCheckingInspection
        hi_message = '¡Bienvenido, ' + username + '! Usa los diferentes ' \
                                                  'comandos de este bot para ' \
                                                  'buscar el significado de ' \
                                                  'palabras, expresiones, ' \
                                                  'terminología jurídica o ' \
                                                  'anagramas en el ' \
                                                  'Diccionario de la Real ' \
                                                  'Academia Española.'

        context.bot.send_message(chat_id=chat_id, text=hi_message)
        print('Bot started normally.')

        global bot_stopped
        bot_stopped = False
        start_called = True
        first_start = False
        second_start = False

    elif start_called and not first_start and not second_start:
        # noinspection SpellCheckingInspection
        hi_message = 'No des más la lata, ya estoy activo. No responderé más ' \
                     'al comando /start. Usa mis comandos o ' \
                     'consulta la ayuda con /help.'
        context.bot.send_message(chat_id=chat_id, text=hi_message)
        print('Alt-intro message sent.')

        second_start = True


# noinspection SpellCheckingInspection
@send_typing_action
def helper(update, context):
    """Send a full description of the bot usage and its commands when the
    command /help is issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    global bot_stopped
    if not bot_stopped:

        username = update.message.from_user.username

        hi = '¡Buenas, ' + username + ', soy @DiccionarioRAEbot! '
        purpose = 'Doy el significado de palabras, expresiones, ' \
                  'americanismos, y nomenclatura jurídica, así como acceso al ' \
                  '' \
                  'diccionario panhispánico de dudas y a la búsqueda de ' \
                  'anagramas.\n '
        available = '\n' + '<b>Comandos disponibles</b>' + '\n'

        commands = '' \
                   '/start - Breve introducción sobre el bot.\n' \
                   '/help - Explicación detallada de comandos y funciones ' \
                   'del bot.\n' \
                   '/anagrama <i>palabra</i> - Encuentra todos los anagramas ' \
                   'de dicha palabra.\n' \
                   '/definiciones <i>palabra</i> - Encuentra todos las ' \
                   'definiciones de dicha palabra.\n' \
                   '/expresiones <i>palabra</i> - Encuentra todos las ' \
                   'expresiones de dicha palabra.\n' \
                   '/stop - Detiene todas las funciones del bot.\n' \
                   '/dona - Mantén este bot y su servidor vivos con una ' \
                   'donación en Paypal.'

        extra = 'Disfruta de este bot. Toda donación será bien recibida y ' \
                'será destinada al completo al mantenimiento del servidor de ' \
                '@DiccionarioRAEbot!'
        help_text = hi + purpose + available + commands + extra

        update.message.reply_text(help_text, parse_mode=ParseMode.HTML)


# def echo(update, context):
#     """Answers to normal messages written to the bot or in the group.
#
#     Args:
#         update: Gets the information of what was sent to the bot.
#         context: Allows reactions from the bot.
#     """
#
#     global bot_stopped
#     if not bot_stopped:
#         username = update.message.from_user.username
#         reply = update.message.text
#         chat_id = update.effective_chat.id


@send_typing_action
def anagram(update, context):
    """Finds all anagrams from the provided word(s) when the
    command /anagrama is issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    global bot_stopped
    if not bot_stopped:

        argument = ' '.join(context.args).lower().strip()
        print('Searching anagrams for', argument)
        # noinspection SpellCheckingInspection
        answer = 'Formato incorrecto. Introduce una única palabra tras el ' \
                 'comando, sin números, símbolos o espacios de por medio.'

        if argument.isalpha():
            answer = get_result(argument, 'anagram')

        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=answer, parse_mode=ParseMode.HTML)


@send_typing_action
def meaning(update, context):
    """Finds all meanings from the provided word(s) when the
    command /definiciones is issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    global bot_stopped
    if not bot_stopped:

        argument = ' '.join(context.args).lower().strip()
        print('Searching meaning for', argument)
        # noinspection SpellCheckingInspection
        answer = 'Formato incorrecto. Introduce una única palabra tras el ' \
                 'comando, sin números, símbolos o espacios de por medio.'
        if argument.isalpha():
            answer = get_result(argument, 'meaning')

        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=answer, parse_mode=ParseMode.HTML)


@send_typing_action
def idiom(update, context):
    """Finds all idioms from the provided word(s) when the
    command /expresiones is issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    global bot_stopped
    if not bot_stopped:

        argument = ' '.join(context.args).lower().strip()
        print('Searching idioms for', argument)
        # noinspection SpellCheckingInspection
        answer = 'Formato incorrecto. Introduce una única palabra tras el ' \
                 'comando, sin números, símbolos o espacios de por medio.'
        if argument.isalpha():
            answer = get_result(argument, 'idiom')

        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=answer, parse_mode=ParseMode.HTML)


@send_typing_action
def stop(update, context):
    """Stops the bot to answer any command other than /start when the
    command /stop is issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    global bot_stopped
    if not bot_stopped:

        # noinspection SpellCheckingInspection
        goodbye_text = 'Me ha sentado mal el pincho de tortilla, ' \
                       'voy a echarme una siesta. Usa /start para ' \
                       'despertarme.'
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=goodbye_text)

        global start_called, first_start
        bot_stopped = True
        start_called = False
        first_start = True
        print('Bot stopped.')


@send_typing_action
def killmeplease(update, context):
    """Kills the bot completely, stopping the script execution, when the
    command /killmeplease is issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    username = update.message.from_user.username

    # noinspection SpellCheckingInspection
    goodbye_text = '¡Ay señor, qué sofocón!. Necesito un médico. Contacta al ' \
                   'creador para volver a usarme.'
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text=goodbye_text)

    print('Bot has been killed by user ', username)
    os.kill(os.getpid(), signal.SIGINT)


@send_typing_action
def donate(update, context):
    """Displays a menu with buttons with different donation amounts when the
    command /dona is issued.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    global bot_stopped
    if not bot_stopped:
        keyboard = donate_keyboard
        reply_markup = InlineKeyboardMarkup(keyboard)
        # noinspection SpellCheckingInspection
        reply = '¿Cuánto quieres donar?'

        update.message.reply_text(reply, reply_markup=reply_markup)
        print('Show donation question and options')


def button(update, context):
    """Understands when a button is pressed.

    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    query = update.callback_query

    query.edit_message_text(text='Selected option: {}'.format(query.data))


def error(update, context):
    """Log Errors caused by Updates.
    Args:
        update: Gets the information of what was sent to the bot.
        context: Allows reactions from the bot.
    """
    logger.warning('Update "%s" caused error "%s"', update, context.error)
