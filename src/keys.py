# -*- coding: utf-8 -*-
"""
Created on 2020-03-06 17:11

@DiccionarioRAEbot for Telegram.
Version 0.4

@author: Arturiki
"""

search2beginning_link = {
    'americanism': 'http://lema.rae.es/damer/?key=',
    'anagram': 'https://dle.rae.es/',
    'doubt': 'https://http://lema.rae.es/dpd/?key=',
    'judicial': 'https://dej.rae.es/dej-lemas/',
    'idiom': 'https://dle.rae.es/',
    'meaning': 'https://dle.rae.es/',
    }
search2end_link = {
    'americanism': '',
    'anagram': '?m=anagram',
    'doubt': '',
    'judicial': '',
    'idiom': '?m=form',
    'meaning': '?m=form',

}

idiom_classes = ['k5', 'k6', 'm']
description_classes = ['n', 'n1', 'n2', 'n3', 'n4', 'n5', 'n6']
definition_classes = ['j', 'j1', 'j2', 'j3', 'j4', 'j5', 'j6']