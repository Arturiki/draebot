# -*- coding: utf-8 -*-
"""
Created on 2020-02-16 11:18

@DiccionarioRAEbot for Telegram.
Version 0.4

@author: Arturiki
"""
from telegram import InlineKeyboardButton


def paypal(amount):
    """Translates a number (given the case) into a string with the currency
    in Euro behind.

    Args:
        amount: Selected amount to donate, either number or string.

    Returns:
        String with amount of money in Euro to be donated.
        """
    link = 'https://www.paypal.me/Arturiki/'
    callback = amount

    if amount.isdigit():
        link = link + amount
        amount = str(amount) + '€'

    option = InlineKeyboardButton(amount, url=link + 'EUR',
                                  callback_data=callback)

    return option


# noinspection SpellCheckingInspection
donate_keyboard = [
    [paypal('5'), paypal('10'), paypal('20')],
    [paypal('Otra cantidad')]
]
