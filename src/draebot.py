# -*- coding: utf-8 -*-
"""
Created on 2020-02-16 11:18

@DiccionarioRAEbot for Telegram.
Version 0.4

@author: Arturiki
"""
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
import commands
import config

updater = Updater(config.TOKEN, use_context=True)


def main():
    """Runs bot and defines the different available commands."""

    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', commands.start))
    dp.add_handler(CommandHandler('help', commands.helper))
    dp.add_handler(CommandHandler('anagrama', commands.anagram))
    dp.add_handler(CommandHandler('definiciones', commands.meaning))
    dp.add_handler(CommandHandler('expresiones', commands.idiom))
    dp.add_handler(CommandHandler('stop', commands.stop))
    dp.add_handler(CommandHandler('donate', commands.donate))
    dp.add_handler(CommandHandler('killmeplease', commands.killmeplease))

    dp.add_handler(CallbackQueryHandler(commands.button))

    dp.add_error_handler(commands.error)

    updater.start_polling()
    print('Bot woke up.')

    # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
    # SIGABRT. This should be used most of the time, since start_polling() is
    # non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()