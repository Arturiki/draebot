# -*- coding: utf-8 -*-
"""
Created on 2020-03-06 17:12

@DiccionarioRAEbot for Telegram.
Version 0.4

@author: Arturiki
"""
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from keys import description_classes, \
    search2beginning_link, search2end_link, definition_classes, idiom_classes


# Significado:
# Funciona perfectamente con "hola"
# Funciona perfectamente con "prueba"
# Funciona perfectamente con "probar"
# Funciona perfectamente con "palabra"
# Funciona perfectamente con "probando" (redirigiendo a "probar")
# Funciona perfectamente con "setas" (devuelve posibles resultados)

# Expresiones:
# Funciona perfectamente con "prueba"
# Funciona perfectamente con "probando" (redirigiendo a "probar")
# Funciona perfectamente con "palabra"
# Funciona perfectamente con "setas" (devuelve posibles resultados)

# Anagramas:
# Funciona perfectamente con "ala"
# Funciona perfectamente con "probando"


def get_link(the_word, the_mode):
    """Gets the link related to the word depending on the mode specified.

    Args:
        the_word: Word to be searched in the link.
        the_mode: The mode determines what kind of link is retrieved.

    Returns:
        String: Link to search a word for a specified mode
        """
    link = search2beginning_link[the_mode] + \
           the_word + \
           search2end_link[the_mode]

    return link


def get_result(the_word, the_mode):
    """Gets the result of searching a word for a specific mode.

    Args:
        the_word: Word to be searched.
        the_mode: The mode in which to search for the word.

    Returns:
        String: Result of the whole search.
        """
    the_link = get_link(the_word, the_mode)
    result = 'No se puede acceder la página web.'

    try:
        the_browser = start_browser(the_link)

        result = mode2method[the_mode](the_browser, the_mode)

        the_browser.quit()
    except NoSuchElementException:
        print('No se puede acceder la página web.')

    return result


def start_browser(the_link):
    """Starts a headless browser and opens a new link .

    Args:
        the_link: Link to be opened.

    Returns:
        Browser with link open link.
        """
    # Wornking exclusively for Raspbian, perhaps Linux too
    CHROMEDRIVER_PATH = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    browser = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH,
                               options=options)
    browser.get(the_link)
    browser.implicitly_wait(60)

    return browser


def is_in_dictionary(the_browser):
    """Check if browser displays whether the word is in the dictionary.

    Args:
        the_browser: Open browser.

    Returns:
        bool: True if in dictionary, False otherwise.
    """
    if 'Aviso' in the_browser.find_element_by_id(
            'resultados').text.split(':', 1)[0]:
        return False
    else:
        return True


def print_items_or_all(the_browser):
    """"Either return the full result or remove the last line of the results
    in case 'Real Academia' is part of them.

    Args:
        the_browser: Open browser.

    Returns:
        String: Full or skimmed result.
    """
    div_items = get_div_items(the_browser)
    result = ''
    if len(div_items) > 0:
        result = result + div_items[0].text
    else:

        result = result + the_browser.find_element_by_id('resultados').text
        if 'Real Academia' in result:
            result = remove_last_line_from_string(result) + ' \n'
    return result


def get_div_items(the_element):
    """Returns all browser elements with class 'item-list'.

    Args:
        the_element: Parent browser element to be searched.

    Returns:
        List: browser elements with the 'item-list' class.
    """
    return the_element.find_elements_by_class_name('item-list')


def remove_last_line_from_string(s):
    """Removes last line from given string.

    Args:
        s: String to be skimmed.

    Returns:
        String: Skimmed string, without last line.
    """
    return s[:s.rfind('\n')]


# def display_similar_or_no_result(the_element):
#     """Checks whether the browser element list is full, and prints all
#     browser elements with the class name, or prints the browser element'.
#
#     Args:
#         the_element: Parent browser element to be searched.
#     """
#     div_items_in_list = the_element.find_elements_by_class_name('item-list')
#
#     if len(div_items_in_list) > 0:
#         print(div_items_in_list[0].text)
#     else:
#         print(the_element.text)
#

def is_correct_class(the_element):
    """Checks whether a browser element has a class attribute.

    Args:
        the_element: Browser element to be checked.

    Returns:
        bool: True if browser element contains a class attribute.
    """
    is_correct = True
    if 'MediaCard' in the_element.get_attribute('class'):
        is_correct = False

    return is_correct


def get_articles_and_children(the_browser, the_mode):
    """For a browser and a mode, checks whether the browser contains
    articles, whether those articles are the wanted kind, and returns the
    text contained in the result article. Otherwise, returns the text
    contained in the browser element with class name 'otras'.

    Args:
        the_browser: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        bool: True if browser element contains a class attribute.
    """
    articles = the_browser.find_elements_by_xpath('//article')
    print('Found articles:', len(articles))
    good_articles = [art for art in articles if is_correct_class(art)]
    print('Found good articles:', len(good_articles))
    result = ''

    if len(good_articles) > 0:
        for article in good_articles:
            result = result + get_header_and_children(article, the_mode) + '\n'
    else:
        others = the_browser.find_element_by_class_name('otras')
        result = others.text

    return result


def get_header_and_children(the_article, the_mode):
    """For an article and a mode, get the article's header, whether those
    articles are the wanted kind, and returns the
    text contained in the result article. Otherwise, returns the text
    contained in the browser element with class name 'otras'.

    Args:
        the_article: Browser article to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The whole text from the result, including the title of the
        result.
    """
    header = the_article.find_element_by_xpath('header')
    result = ''

    if 'Conjugación de ' not in header.text:
        title = header.text
        print('\n' + title)
        result = '<b>' + title + '</b>' + '\n'

        p_elements = the_article.find_elements_by_xpath('p')
        result = result + print_meaning_or_idiom(p_elements, the_mode)

    return result


def print_meaning_or_idiom(parent_element, the_mode):
    """For a browser element and a mode, retrieve the content of the element.
    Depending on the class of the sub-elements, add italics to description
    text, just plain text, nothing or error message.

    Args:
        parent_element: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The whole text from the result, nothing or an error message.
    """
    mode_classes = idiom_classes if the_mode is 'idiom' else definition_classes
    # full_text = ''
    result = ''
    for p_element in parent_element:

        class_name = p_element.get_attribute('class')

        if the_mode is 'meaning' and class_name in description_classes:
            result = result + '<i>' + p_element.text + '</i>' + '\n'
            print(p_element.text)

        elif class_name in mode_classes:
            result = result + p_element.text + '\n'
            print(p_element.text)

        else:
            pass
    if result == '':
        result = 'No hay coincidencias.'

    return result


def get_meaning(the_browser, the_mode):
    """Get the meanings of a word given the browser element and a mode.

    Args:
        the_browser: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The meanings of the word.
    """
    if is_in_dictionary(the_browser):
        print('Word is in dictionary')

        result = get_articles_and_children(the_browser, the_mode)
    else:
        print('Word is NOT in dictionary')

        result = print_items_or_all(the_browser)

    return result


def get_idiom(the_browser, the_mode):
    """Get the idioms of a word given the browser element and a mode.

    Args:
        the_browser: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The idioms of the word.
    """
    result = ''
    if is_in_dictionary(the_browser):
        print('Word is in dictionary')

        result = get_articles_and_children(the_browser, the_mode)
    else:
        print('Word is NOT in dictionary')
        result = print_items_or_all(the_browser)

    return result


def get_doubt(the_browser, the_mode):
    """Get the doubts around a word given the browser element and a mode.

    Args:
        the_browser: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The doubts around the word.
    """
    if is_in_dictionary(the_browser):
        get_articles_and_children(the_browser, the_mode)

    return result


def get_judicial(the_browser, the_mode):
    """Get the judicial terminology of a word given the browser element and a
    mode.

    Args:
        the_browser: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The judicial terminology of the word.
    """
    if is_in_dictionary(the_browser):
        get_articles_and_children(the_browser, the_mode)

    return result


def get_anagram(the_browser, the_mode):
    """Get the anagrams of a word given the browser element and a mode.

    Args:
        the_browser: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The anagrams of the word.
    """
    result = print_items_or_all(the_browser)

    return result


def get_americanism(the_browser, the_mode):
    """Get the americanisms of a word given the browser element and a mode.

    Args:
        the_browser: Browser element to be checked.
        the_mode: Dictionary mode.

    Returns:
        String: The americanisms of the word.
    """
    if is_in_dictionary(the_browser):
        get_articles_and_children(the_browser, the_mode)

    return result


mode2method = {
    'anagram': get_anagram,
    'americanism': get_americanism,
    'doubt': get_doubt,
    'judicial': get_judicial,
    'idiom': get_idiom,
    'meaning': get_meaning,
    }
